if (global.invincible == 0)
{
    audio_play_sound(Explosion_nave,100,false);
    var o;
    o = instance_create(x,y,obj_Explosion);
    instance_destroy();
    global.pLives -= 1;
    if(global.pLives < 0)
    {
        global.state = 1;
    }
}
